package Paquete;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
public class Metodos
{
    public Orden InsertarO()
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Orden orden1 = new Orden();
        java.util.Date utilDate=new java.util.Date();
        java.sql.Date sqlDate=new java.sql.Date(utilDate.getTime());
        orden1.setFechaOrden(sqlDate);
        try
        {
            em.persist(orden1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return orden1;
    }
    public Producto InsertarP(String descripcion,Float preciop)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Producto producto1 = new Producto();
        producto1.setDescripcion(descripcion);
        Float precio=Float.valueOf(preciop);
        producto1.setPrecio(precio);
        try
        {
            em.persist(producto1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
            System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return producto1;
    }
    public Lineaorden InsertarLO(Orden orden,Producto producto,int cantidad)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        LineaordenPK lineaordenPK1 = new LineaordenPK(orden.getIdOrden(),producto.getIdProducto());
        Lineaorden lineaorden1 = new Lineaorden(orden.getIdOrden(),producto.getIdProducto());
        lineaorden1.setCantidad(cantidad);
        
        try
        {
            em.persist(lineaorden1);
        }
        catch (Exception e)
        {
        }
        try
        {
            em.getTransaction().commit();
        }
        catch (Exception e)
        {
 	           System.out.println("Ocurrio el error" + e);
        }
        em.close();
        emf.close();
        return lineaorden1;
    }
    public void Mostrar(Orden orden,Producto producto,Lineaorden lineaorden)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Orden> lista_orden;
        lista_orden = em.createNamedQuery("Orden.findAll").getResultList();
        System.out.println("--------------------Orden----------------------");
        for (Orden el : lista_orden)
        {
            System.out.println("ID orden: "+el.getIdOrden()+"\nFecha orden: "+el.getFechaOrden());
            System.out.println("*");
        
        }        
        System.out.println("-------------------Producto--------------------");
        Collection<Producto> lista_producto;
        lista_producto = em.createNamedQuery("Producto.findAll").getResultList();

        for (Producto el : lista_producto)
        {
            System.out.println("ID producto: "+el.getIdProducto()+"\nDescripcion producto: "+el.getDescripcion()+"\nPrecio: "+el.getPrecio());
            System.out.println("*");
        }
        System.out.println("------------------Linea Orden------------------");
        Collection<Lineaorden> lista_lineaorden;
        lista_lineaorden = em.createNamedQuery("Lineaorden.findAll").getResultList();

        for (Lineaorden el : lista_lineaorden)
        {
            System.out.println("ID orden: "+el.getOrden().getIdOrden()+"\nID producto: "+el.getProducto().getIdProducto()+"\nCantidad: "+el.getCantidad());
            System.out.println("*");
        }
        System.out.println("-----------------------------------------------");
    }
}

































/*
        System.out.println("------------Datos Orden-------------");
        System.out.println("ID orden: "+orden.getIdOrden());
        System.out.println("ID orden: "+orden.getFechaOrden());
        System.out.println("------------Datos Producto----------");
        System.out.println("ID orden: "+producto.getIdProducto());
        System.out.println("ID orden: "+producto.getDescripcion());
        System.out.println("ID orden: "+producto.getPrecio());
        System.out.println("------------Datos Lineaorden--------");
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdOrden());
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdProducto());
        System.out.println("ID orden: "+lineaorden.getCantidad());
        */